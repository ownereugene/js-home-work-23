const input = document.createElement("input");
const inputWrapper = document.querySelector(".input-wrapper");
const label = document.createElement("label");
const errorSpan = document.createElement("span");
const spanPrice = document.createElement("span");

function priceEnter() {
  label.innerHTML = "Price, $:";
  label.append(input);
  inputWrapper.append(label);
}

input.addEventListener("focus", () => {
  if (input.value < 0 || input.value === "" || isNaN(Number(input.value))) {
    input.style.outline = "none";
    input.style.boxShadow = "0 0 10px green";
    input.style.color = "";
    errorSpan.remove();
  }
});

input.addEventListener("blur", () => {
  if (input.value < 0 || isNaN(Number(input.value))) {
    errorSpan.innerHTML = "Please enter correct price";
    errorSpan.style.color = "red";
    input.style.color = "red";
    input.style.boxShadow = "0 0 10px red";
    label.after(errorSpan);
  } else if (input.value === "") {
    input.style.boxShadow = "";
  } else {
    label.remove();
    spanPrice.classList.add("price");
    spanPrice.innerHTML = `Поточна ціна: ${input.value}$<i class="fa-solid fa-xmark"></i>`;
    spanPrice.style.boxShadow = '0px 0px 10px 1px green';
    inputWrapper.append(spanPrice);
  }
});

priceEnter();

spanPrice.addEventListener("click", (event) => {
  const iClose = document.querySelector("i");
  if (event.target === iClose) {
    input.style.boxShadow = '';
    input.value = "";
    spanPrice.remove();
    priceEnter();
  }
});
